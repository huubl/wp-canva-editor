<?php
/**
 * Editor. Assets.
 *
 * @package WPDesk\Library\WPCanvaEditor
 */

namespace WPDesk\Library\WPCanvaEditor;

use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Enqueue editor scripts.
 *
 * @package WPDesk\Library\WPCanvaEditor
 */
class Assets implements Hookable {

    /**
     * @var string
     */
    private $post_type;

    /**
     * @var string
     */
    protected $scripts_version = '1.1';

    /**
     * @param $post_type
     */
    public function __construct( $post_type ) {
        $this->post_type = $post_type;
    }

    /**
     * Fires hooks
     */
    public function hooks() {
        add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
    }

    /**
     * @return string
     */
    protected function get_assets_url() {
        return trailingslashit( plugin_dir_url( __DIR__ ) ) . 'assets/';
    }

    /**
     * Enqueue editor scripts and styles.
     */
    public function admin_enqueue_scripts() {
        $screen = get_current_screen();
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

        $is_pl   = 'pl_PL' === get_locale();
        $pro_url = $is_pl ? 'https://www.wpdesk.pl/sklep/flexible-coupons-woocommerce/?utm_source=flexible-coupons-product-edition&amp;utm_medium=link&amp;utm_campaign=flexible-coupons-pro' : 'https://www.wpdesk.net/products/flexible-coupons-woocommerce/?utm_source=flexible-coupons-product-edition&amp;utm_medium=link&amp;utm_campaign=flexible-coupons-pro';

        if ( 'post' === $screen->base && $this->post_type === $screen->post_type ) {

            wp_enqueue_style( 'admin', $this->get_assets_url() . 'css/admin.css', [], $this->scripts_version );

            wp_enqueue_media();
            wp_register_script( 'wp-canva-editor', $this->get_assets_url() . 'js/wpdesk-canva-editor' . $suffix . '.js', array( 'wp-editor' ), $this->scripts_version, true );
            wp_enqueue_script( 'wp-canva-editor' );
            wp_localize_script(
                'wp-canva-editor',
                'wpdesk_canva_editor_lang',
                array(
                    'general'              => esc_html__( 'General', 'wp-canva-editor' ),
                    'images'               => esc_html__( 'Images', 'wp-canva-editor' ),
                    'text'                 => esc_html__( 'Text', 'wp-canva-editor' ),
                    'shortcodes'           => esc_html__( 'Shortcodes', 'wp-canva-editor' ),
                    'select_format'        => esc_html__( 'Select format', 'wp-canva-editor' ),
                    'page_orientation'     => esc_html__( 'Page orientation', 'wp-canva-editor' ),
                    'vertical'             => esc_html__( 'Vertical', 'wp-canva-editor' ),
                    'horizontal'           => esc_html__( 'Horizontal', 'wp-canva-editor' ),
                    'background_color'     => esc_html__( 'Background color', 'wp-canva-editor' ),
                    'color'                => esc_html__( 'Color', 'wp-canva-editor' ),
                    'select'               => esc_html__( 'Select', 'wp-canva-editor' ),
                    'select_images'        => esc_html__( 'Select images', 'wp-canva-editor' ),
                    'add_to_area'          => esc_attr__( 'Add to area', 'wp-canva-editor' ),
                    'remove_from_project'  => esc_attr__( 'Remove from project', 'wp-canva-editor' ),
                    'edit'                 => esc_attr__( 'Edit', 'wp-canva-editor' ),
                    'fit_to_screen'        => esc_attr__( 'Fit to screen', 'wp-canva-editor' ),
                    'crop'                 => esc_attr__( 'Crop', 'wp-canva-editor' ),
                    'layer_up'             => esc_attr__( 'Layer up', 'wp-canva-editor' ),
                    'layer_down'           => esc_attr__( 'Layer down', 'wp-canva-editor' ),
                    'clone_element'        => esc_attr__( 'Clone element', 'wp-canva-editor' ),
                    'delete_element'       => esc_attr__( 'Delete element', 'wp-canva-editor' ),
                    'delete'               => esc_attr__( 'Delete element', 'wp-canva-editor' ),
                    'change_image'         => esc_attr__( 'Change image', 'wp-canva-editor' ),
                    'align_right'          => esc_attr__( 'Align right', 'wp-canva-editor' ),
                    'align_left'           => esc_attr__( 'Align left', 'wp-canva-editor' ),
                    'align_center'         => esc_attr__( 'Align center', 'wp-canva-editor' ),
                    'justify'              => esc_attr__( 'Justify', 'wp-canva-editor' ),
                    'font_size'            => esc_attr__( 'Font size', 'wp-canva-editor' ),
                    'change_font_size'     => esc_attr__( 'Change font size', 'wp-canva-editor' ),
                    'font_family'          => esc_attr__( 'Font family', 'wp-canva-editor' ),
                    'change_font_family'   => esc_attr__( 'Change font family', 'wp-canva-editor' ),
                    'double_click_to_edit' => esc_attr__( 'Double click to edit', 'wp-canva-editor' ),
                    'font_color'           => esc_attr__( 'Font color', 'wp-canva-editor' ),
                    'header1'              => esc_html__( 'Header 1', 'wp-canva-editor' ),
                    'header2'              => esc_html__( 'Header 2', 'wp-canva-editor' ),
                    'header3'              => esc_html__( 'Header 3', 'wp-canva-editor' ),
                    'content'              => esc_html__( 'Content', 'wp-canva-editor' ),
                    'upgrade_to_pro'       => esc_html__( 'Upgrade to PRO and enable more shortcodes →', 'wp-canva-editor' ),
                    'pro_url'              => $pro_url,
                )
            );

            wp_register_script( 'wp-canva-admin', $this->get_assets_url() . 'js/admin.js', array( 'wp-canva-editor' ), $this->scripts_version, true );
            wp_enqueue_script( 'wp-canva-admin' );
            wp_localize_script(
                'wp-canva-admin',
                'wp_canva_admin',
                array(
                    'post_type' => $this->post_type,
                    'nonce'     => wp_create_nonce( 'editor_save_post_' . $this->post_type ),
                    'lang'      => get_locale(),
                )
            );
        }
    }

}

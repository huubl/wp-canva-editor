<?php
$editor_data = isset( $editor_data ) ? $editor_data : [];
?>
<script>
    window.WPDeskCanvaEditorData = <?php echo json_encode( $editor_data, JSON_NUMERIC_CHECK ); ?>;
</script>
<div class="publishing-actions-box">
    <div class="black-box"></div>
    <a href="<?php echo esc_url( $pro_url ); ?>" class="button button-primary button-large wpdesk-button pro-button" target="_blank"><?php esc_html_e( 'Buy PRO Version', 'wp-canva-editor' ); ?></a>
    <span class="process_save"><span id="process_save_template"></span><span class="spinner"></span></span>
    <input name="save_wpdesk_canva_template" type="button" class="button button-primary button-large" id="save_wpdesk_canva_template" value="<?php esc_attr_e( 'Save template', 'wp-canva-editor' ); ?>">
</div>
<div id="wpdesk-canva-root"></div>
<input type="hidden" id="editor_post_id" name="post_ID" value="<?php echo isset( $post->ID ) ? $post->ID : ''; ?>"/>
<div class="editor-objects-dev" style="display:none">
    <?php print_r( $editor_data ); ?>
</div>

import React from 'react';
import { ProVersion, ListParent, ListItem } from "../StyledComponents";
import {lang, shortcode_list } from '../Helpers';
import {ShortcodeData} from "../Context/EditorDefaultData";

class Shortcodes extends React.Component {

    constructor(props) {
        super(props);
    }

    addShortcode = (event, object) => {
        this.props.addObjectToArea(event, object)
        this.props.closeMenu(null);
    }

    render() {

        let shortcodes = shortcode_list();
        const ShortcodeItems = shortcodes.map(( object, key) => {
            let shortcode_props = {...ShortcodeData, ...object};
            return (
                <ListItem
                    key={key}
                    title={lang('add_to_area')}
                    onClick={ (event) => this.addShortcode( event, { ...shortcode_props } )}>
                    {shortcode_props.text}
                </ListItem>
            )
        });

        let ProVersionLink;
        if (shortcodes.length < 3) {
            ProVersionLink = <ProVersion><a target="_blank" href={lang('pro_url')}>{lang('upgrade_to_pro')}</a></ProVersion>
        } else {
            ProVersionLink = '';
        }

        return (
            <React.Fragment>
                <ListParent>
                    {ShortcodeItems}
                    {ProVersionLink}
                </ListParent>
            </React.Fragment>
        );
    }

}

export default Shortcodes;

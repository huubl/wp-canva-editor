import {MenuTextWrapper, MenuTextWrapperItem} from "./StyledComponents";
import React from "react";
import {FontsData} from "./Context/EditorDefaultData";

export function lang(key) {
    if (typeof wpdesk_canva_editor_lang !== 'undefined' && wpdesk_canva_editor_lang.hasOwnProperty(key)) {
        return wpdesk_canva_editor_lang[key];
    }

    return key;
}

export function shortcode_list() {
    if (typeof wpdesk_canva_editor_shortcodes !== 'undefined') {
        return wpdesk_canva_editor_shortcodes;
    }

    return [
        {
            text: '[coupon_price]',
            top: 10,
            left: 10,
        },
        {
            text: '[coupon_code]',
            top: 120,
            left: 10,
        },
    ];
}

export function fonts_list() {
    if (typeof wpdesk_canva_editor_fonts !== 'undefined') {
        return wpdesk_canva_editor_fonts;
    }

    return FontsData;
}

export function texts_list() {
    let default_texts = [
        {
            type: 'text',
            name: lang('header1'),
            text: lang('header1'),
            tag: 'h1',
            fontSize: 36,
            fontWeight: 700,
            height: 50,
            width: 200,
            top: 50,
        },
        {
            type: 'text',
            name: lang('header2'),
            text: lang('header2'),
            tag: 'h2',
            fontSize: 30,
            fontWeight: 700,
            height: 50,
            width: 200,
            top: 100,
        },
        {
            type: 'text',
            name: lang('header3'),
            text: lang('header3'),
            tag: 'h3',
            fontSize: 24,
            fontWeight: 700,
            height: 50,
            width: 200,
            top: 150,
        },
        {
            type: 'text',
            name: lang('content'),
            tag: 'p',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo turpis sit amet sapien fringilla placerat. Curabitur condimentum euismod neque, id fermentum elit posuere ut.',
            height: 100,
            width: 400,
            top: 200,
        },
    ];

    if (typeof wpdesk_canva_editor_texts !== 'undefined' ) {
        return [ ...default_texts, ...wpdesk_canva_editor_texts ];
    }

    return default_texts;
}

export function updateAuxlineLeft(object) {
    let element = document.getElementById('auxLineLeft');
    element.style.top = '0';
    element.style.left = object.left + 'px';
    element.style.display = object.display;
}

export function updateAuxlineRight(object) {
    let element = document.getElementById('auxLineRight');
    element.style.top = '0';
    element.style.left = object.left + 'px';
    element.style.display = object.display;
}

export function updateAuxlineTop(object) {
    let element = document.getElementById('auxLineTop');
    element.style.left = '0';
    element.style.top = object.top + 'px';
    element.style.display = object.display;
}

export function updateAuxlineBottom(object) {
    let element = document.getElementById('auxLineBottom');
    element.style.left = '0';
    element.style.bottom = object.bottom + 'px';
    element.style.display = object.display;
}

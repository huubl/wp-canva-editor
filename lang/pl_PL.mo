��    2      �  C   <      H     I     U  
   b     m     y     �     �     �     �     �     �     �     �     �               %     *     1     F  
   T     _  	   k     u     }     �     �  
   �     �     �  
   �     �     �     �     �                         -  
   ;  1   F     x  -   }     �     �     �     �        	       )
  
   :
     E
     X
  
   l
     w
     �
     �
     �
     �
     �
     �
     �
     �
       #        5     <     C     Y     n     }     �     �     �     �     �     �     �     �     �     �          "     5     F     U     ^     f     u  	   �  +   �     �  7   �     �                                     '   *   +                %   "                    2      1      #          /                                0   )   .      ,   (                       -   	          
      &   !                                   $           Add to area Align center Align left Align right Background color Buy PRO Version Change font family Change font size Change image Clone element Color Content Crop Data cannot be saved Delete element Double click to edit Edit Editor Enter template title Fit to screen Font color Font family Font size General Header 1 Header 2 Header 3 Horizontal Images Justify Layer down Layer up Manage editor templates. Page orientation Remove from project Save template Saved Select Select format Select images Shortcodes Template does not contain [coupon_code] shortcode Text Upgrade to PRO and enable more shortcodes → Vertical add new on admin barEditor admin menuEditor post type general nameEditor post type singular nameEditor Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-22 22:16+0200
Last-Translator: Krzysztof Dyszczyk <krzysztof.dyszczyk@gmail.com>
Language-Team: Maciej Swoboda <maciej.swoboda@gmail.com>
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Generator: Poedit 2.3
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SearchPathExcluded-1: vendor
 Dodaj do obszaru Wyśrodkuj Wyrównaj do lewej Wyrównaj do prawej Kolor tła Kup wersję PRO Zmień krój czcionki Zmień rozmiar czcionki Zmień obraz Klonuj element Kolor Treść Przytnij Nie można zapisać danych Usuń element Podwójne kliknięcie edytuje tekst Edytuj Edytor Wpisz tytuł szablonu Wyrównaj do obszaru Kolor czcionki Krój czcionki Rozmiar czcinki Główne Nagłówek 1 Nagłówek 2 Nagłówek 3 Pozioma Obrazki Wyjustuj Warstwa niżej Warstwa wyżej Zarządzaj szablonami edytora. Orientacja wydruku Usuń z projektu Zapisz szablon Zapisano Wybierz Wybierz format Wybierz obrazki Szortkody Szablon nie zawiera shortkoda [coupon_code] Tekst Kup wersję PRO aby korzystać z innych szortkodów → Pionowa Edytor Edytor Edytor Edytor 
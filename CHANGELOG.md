## [1.2.0] - 2020-06-18
### Fixed
- fixed problems with coupon implementation
- refactor edges detector for all objects inside area when selected object is resized or moved.
- added shared interfaces

## [1.1.1] - 2020-06-10
### Fixed
- remove unused code, refactor menu text objects

## [1.1.0] - 2020-06-06
### Fixed
- implements custom texts

## [1.0.0] - 2020-06-06
### Added
- init
